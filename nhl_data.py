import json

from player import Player

class NhlData:
    def __init__(self):
        self.__players = []
    
    def read_json_file(self, name):
        try:
            with open("data/" + name) as my_file:
                data = my_file.read()
        except FileNotFoundError:
            print("No such file")
            return
        players = json.loads(data)
        for player in players:
            new_player = Player(player["name"], player["nationality"], int(player["assists"]), int(player["goals"]), int(player["penalties"]), player["team"], int(player["games"]))
            self.__players.append(new_player)
        print(f"read the data of {len(self.__players)} players")
        return True
    
    def get_all_players(self):
        return self.__players
    
    def search_player(self, name):
        return list(filter(lambda player: player.name == name, self.__players))
    
    def get_all_teams(self):
        return sorted(list(set([player.team for player in self.__players])))

    def get_all_countries(self):
        return sorted(list(set([player.nationality for player in self.__players])))

    def get_players_from_team(self, team):
        return sorted([player for player in self.__players if player.team == team], key=lambda player: player.assists + player.goals, reverse=True)

    def get_players_from_country(self, country):
        return sorted([player for player in self.__players if player.nationality == country], key=lambda player: player.assists + player.goals, reverse=True)

    def get_most_points_scorers(self, how_many):
        if how_many <= len(self.__players):
            all = sorted([player for player in self.__players], key=lambda player: (player.assists + player.goals, player.goals), reverse=True)[0:how_many]
        else: 
            raise ValueError
        return all
    
    def get_most_goals_scored(self, how_many):
        if how_many <= len(self.__players):
            all = sorted([player for player in self.__players], key=lambda player: (player.goals, -player.games), reverse=True)[0:how_many]
        else:
            raise ValueError
        return all

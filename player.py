import itertools

class Player:
    id = itertools.count(1)

    def __init__(self, name: str, nationality: str, assists: int, goals: int, penalties: int, team: str, games: int):
        self.name = name
        self.nationality = nationality
        self.assists = assists
        self.goals = goals
        self.penalties = penalties
        self.team = team
        self.games = games
        self.id = next(Player.id)
   
    def __str__(self):
        return f"{self.name:21}{self.team:>}{self.goals:>4} {'+'} {self.assists:>2} {'=':>1} {self.goals + self.assists:>3}"


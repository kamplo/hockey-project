# Hockey statistics application

Simple python3 app which parses json data and provides user a functionality to browse NHL statistics. Created as an answer to exercise from python advanced online course.

Contains examples of:

* list comprehesions
* functions as arguments
* lambdas
* and other operations on lists

Error handling and tests are not implemented.

## Usage:
1. Run app: 'python3 nhl_application.py'
2. Enter file name, e.g. all.json
3. Enter command number and follow the instructions
from nhl_data import NhlData

class NhlApplication:
    nhl_data = NhlData()

    def execute(self):
        filename = input("file name: ")
        is_file_parsed = self.nhl_data.read_json_file(filename)
        if not is_file_parsed:
            return
        self.help()
        while True:
            print("")
            command = input("command: ")
            if command == "0":
                break
            elif command == "1":
                self.search_player()
            elif command == "2":
                self.print_teams()
            elif command == "3":
                self.print_countries()
            elif command == "4":
                self.list_players_from_team()
            elif command == "5":
                self.list_players_from_country()
            elif command == "6":
                self.list_most_points()
            elif command == "7":
                self.list_most_goals()
            else:
                self.help()

    def help(self):
        print("commands: ")
        print("0 quit")
        print("1 search for player")
        print("2 teams")
        print("3 countries")
        print("4 players in team")
        print("5 players from country")
        print("6 most points")
        print("7 most goals")
    
    def search_player(self):
        name = input("name: ")
        search_results = self.nhl_data.search_player(name)
        if search_results:
            print(search_results[0])
        else:
            print("Search returned no results")

    def print_teams(self):
        for team in self.nhl_data.get_all_teams():
            print(team)
    
    def print_countries(self):
        for c in self.nhl_data.get_all_countries():
            print(c)
    
    def list_players_from_team(self):
        team = input("team: ")
        for p in self.nhl_data.get_players_from_team(team):
            print(p)

    def list_players_from_country(self):
        cty = input("country: ")
        for p in self.nhl_data.get_players_from_country(cty):
            print(p)

    def list_most_points(self):
        number_of_records = input("how many: ")
        for p in self.nhl_data.get_most_points_scorers(int(number_of_records)):
            print(p)

    def list_most_goals(self):
        number_of_records = input("how many: ")
        for p in self.nhl_data.get_most_goals_scored(int(number_of_records)):
            print(p)


app = NhlApplication()
app.execute()